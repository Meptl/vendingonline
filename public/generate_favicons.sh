#!/bin/sh
convert ../src/images/mario-coin.png -background none -define icon:auto-resize=256,64,48,32,16 favicon.ico
convert ../src/images/mario-coin.png -resize 192x192 -background none logo192.png
convert ../src/images/mario-coin.png -resize 512x512 -background none logo512.png
