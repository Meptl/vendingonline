FROM node:alpine as builder
WORKDIR /app

# Add these first to improve caching.
COPY ./package-lock.json ./package.json ./
RUN npm install

COPY . ./
RUN npm run build

FROM nginx:alpine
COPY deployment/nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=builder /app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
