import React from 'react';
import coin_image from './images/mario-coin.png';
import './coin.css';
import CoinImg from './coin.style';
import Draggable from 'react-draggable';
import { CSSTransitionGroup } from 'react-transition-group';

class Coin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      draggable: true,
      finalAnimation: 'slide-out-bottom',
    }
    this.imgRef = React.createRef();
  }

  inDropRegion(x, y) {
    const my_rect = this.imgRef.current.getBoundingClientRect();
    const center = {
      x: (my_rect.left + my_rect.right) / 2.0,
      y: (my_rect.top + my_rect.bottom) / 2.0,
    };

    const drop_region = this.props.dropRegionRef.current.getBoundingClientRect();
    return  center.x > drop_region.left &&
            center.x < drop_region.right &&
            center.y > drop_region.top &&
            center.y < drop_region.bottom;
  }

  /** react-draggable works by having an inline style of translate(x, y). The
   * underlying DOM objects positions are still fixed. This makes it play
   * poorly with CSS animations. After dragging, resolve the new position with
  /* top and left CSS attributes.
   */
  resolveFinalPosition() {
    // When there is no y translation the inline style is similar to
    // translate(4px)
    const regexp = /translate\((-?\d+)px(, (-?\d+)px\))?/g;
    const matches = [...this.imgRef.current.style.transform.matchAll(regexp)];
    // Clear the inline translate(x) from Draggable.
    this.imgRef.current.style = null;

    const final_x = this.imgRef.current.x + parseInt(matches[0][1]);
    let final_y = this.imgRef.current.y;
    if (matches[0][3]) {
      final_y += parseInt(matches[0][3]);
    }

    this.imgRef.current.style.setProperty('left', final_x + 'px');
    this.imgRef.current.style.setProperty('top', final_y + 'px');
  }

  handleDragStop() {
    this.resolveFinalPosition();

    let finalAnimation;
    if (this.inDropRegion()) {
      finalAnimation = 'scale-out-right';
    } else {
      finalAnimation = 'slide-out-bottom';
    }

    this.setState({
      draggable: false,
      finalAnimation: finalAnimation,
    });

  }

  render() {
    const hideDragHint = () => this.props.dragHintRef.current.style.setProperty('opacity', '0');
    let coin;
    if (this.state.draggable) {
      coin = (
        <Draggable onStart={hideDragHint} onStop={() => this.handleDragStop()}>
          <CoinImg
            src={coin_image}
            alt=''
            ref={this.imgRef}
            draggable='false'
            startPos={this.props.startPos}
          />
        </Draggable>
      );
    }

    // For whatever reason the transition names feature isn't working for the
    // leave state. It only applies the '-active' variant of leave and crashes
    // on leaveActive. The matching css file for this class uses the "-active"
    // selector for the leave state.
    let transitions = {
      appear: 'bounce-in-top',
      leave: this.state.finalAnimation,
    }
    return (
        <CSSTransitionGroup
          transitionName={transitions}
          transitionEnterTimeout={500}
          transitionAppear={true}
          transitionAppearTimeout={500}
          transitionLeave={!this.state.draggable}
          transitionLeaveTimeout={25500}
        >
          { coin }
        </CSSTransitionGroup>
    )
  }
}

export default Coin;
