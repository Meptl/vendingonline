import { PayPalScriptProvider, PayPalButtons } from "@paypal/react-paypal-js";
const  payPalOptions = {
  "client-id": "AXlZvhU3YRtyTNnsgD29kvCoy5YWLYHiANGeEHwil6NJ-zCMw5UfOcggXlC0gc0It7CvhNzByz6LUawT",
};

const createOrder = (data, actions) => {
  return actions.order.create({
    purchase_units: [
      {
        amount: {
          value: "0.01"
        },
      },
    ],
    application_context: {
      shipping_preference: "NO_SHIPPING"
    }
  });
}

const ShoppingCart = (props) => {
  return (
    <PayPalScriptProvider options={payPalOptions}>
      <PayPalButtons
        style={{ layout: "horizontal", shape: "pill", color: "white" }}
        createOrder={createOrder}
        onApprove={(data, action) => props.addCoin()}
      />
    </PayPalScriptProvider>
  );
}
export default ShoppingCart;
